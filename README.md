This is the new website. It consists of an updated and better organized version than the previous one. The other one we will call it version 1.0.0. This new website is v 2.0.0. Minor additions and
changes will be denoted by v 2.x.x. 

In order to see this website, download the git repository and open the index.html file in your browser.

Note: For addition of images, these are organized depending on the section of the website that 
you are trying to change. For instance, the jumbotron or 'main image' is under images/jumbotron. Same follows for the other cases. 

The objective for now is to create the main html file. From there, we can develop and create a multi-page application where we will incorporate new files such as the eboard.html, sponsors.html, which these consist of extended descriptions of the main html file. The index.html should have all the extended documents in as a brief description. This means that if you create an about.html file, a small description should added to the index.html (main file). 

Once, everything is added, it is recommended to add an angular.js framework to the website to decouple elements and for optimization reasons. We need to ensure that the server is dealt with the minimum amount of work possible. Note that angular.js can be used for portions of the website. This framework does not need to be applied for the whole page. Other front-end options would be react.js.

After that, we would like to incorporate a database with the information of each member, which will
definitely reduce the amount of work from the treasurer and the secretary, as well as having more
information about each member in order to analyze preferences and create statistical models. 

To deal with the server side, use node.js and npm (node package manager) to configure and manage
the dependencies of the website. 

------------------------------------------------------------------------------------------
Sponsor Images
------------------------------------------------------------------------------------------

Images need to be the size of 165x165, ALL OF THEM. They have already been compressed for maximum efficiency. Once they are the same size, correct padding and margins to make them straight and symmetric.

------------------------------------------------------------------------------------------
Contact information

PD: Still information to add, but this should be enough. If you need to contact me for more information please do so at iariza3@gatech.edu. 
